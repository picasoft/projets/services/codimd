#! /bin/sh

cat << EOF > /home/codimd/app/crontab
$DELETE_AT /home/codimd/app/deleteOldPad.py
EOF
if ! grep -q "https://picasoft.net/co/cgu.html" /home/codimd/app/public/views/index/body.ejs; then
  sed "154a  | <a href=\"https://picasoft.net/co/cgu.html\">CGU</a> " -i /home/codimd/app/public/views/index/body.ejs
fi
if ! grep -q "deux ans après la dernière modification" /home/codimd/app/public/views/index/body.ejs; then
  sed "41a <span style=\"font-size:2em;font-weight:bold;\">Cette instance ne garde le contenu que deux ans après la dernière modification !</span>" -i /home/codimd/app/public/views/index/body.ejs
fi
/home/codimd/app/docker-entrypoint.sh &
/home/codimd/app/supercronic /home/codimd/app/crontab
