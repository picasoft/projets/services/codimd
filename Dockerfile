ARG VERSION
ARG PORTCHECKER_VERSION
ARG SCVERSION

FROM node:12-alpine3.15 as BUILD
ARG VERSION
ARG SCVERSION
RUN apk add --no-cache --virtual .gyp libressl-dev git bash python3 make && \
    wget https://github.com/hackmdio/codimd/archive/${VERSION}.tar.gz && \
    tar xzf $VERSION.tar.gz -C /opt && \
    mv /opt/codimd-$VERSION /opt/codimd && \
    wget https://github.com/aptible/supercronic/releases/download/$SCVERSION/supercronic-linux-amd64 && \
    chmod +x supercronic-linux-amd64 && \
    mv supercronic-linux-amd64 /opt/codimd/supercronic
COPY fr.dic /opt/codimd/public/vendor/codemirror-spell-checker/fr.dic
COPY fr.aff /opt/codimd/public/vendor/codemirror-spell-checker/fr.aff
COPY spellcheck.js /opt/codimd/public/js/lib/editor/spellcheck.js
RUN cd /opt/codimd && npm install && \
    npm run build && \
    cp ./deployments/docker-entrypoint.sh ./ && \
    cp .sequelizerc.example .sequelizerc && \
    rm -rf .git .gitignore .travis.yml .dockerignore .editorconfig .babelrc .mailmap .sequelizerc.example \
        test docs contribute \
        package-lock.json webpack.prod.js webpack.htmlexport.js webpack.dev.js webpack.common.js \
        config.json.example README.md CONTRIBUTING.md AUTHORS node_modules

FROM node:12-alpine3.15
ARG PORTCHECKER_VERSION
RUN addgroup --gid 5010 codimd && \
    adduser -u 5010 -G codimd -D codimd && \
    mkdir /home/$USER_NAME/.npm && \
    echo "prefix=/home/codimd/.npm/" > /home/$USER_NAME/.npmrc && \
    mkdir -p /home/codimd/app/deletedPads && \
    chown -R codimd:codimd /home/codimd && \
    # Git needed so NPM can clone packages and Python for node-gyp
    apk add --no-cache bash git python3-dev py3-pip gcc postgresql-dev musl-dev make && \
    pip3 install psycopg2 && \
    wget https://github.com/hackmdio/portchecker/releases/download/${PORTCHECKER_VERSION}/portchecker-linux-amd64.tar.gz && \
    tar xvf portchecker-linux-amd64.tar.gz -C /usr/local/bin && \
    mv /usr/local/bin/portchecker-linux-amd64 /usr/local/bin/pcheck && \
    rm portchecker-linux-amd64.tar.gz
USER codimd
WORKDIR /home/codimd/app
COPY --chown=5010:5010 --from=BUILD /opt/codimd .
COPY --chown=5010:5010 deleteOldPad.py .
RUN npm install --production && npm cache clean --force && rm -rf /tmp/{core-js-banners,phantomjs}
COPY entrypoint.sh .
COPY features.md /home/codimd/app/public/docs/features.md
EXPOSE 3000
ENTRYPOINT ["./entrypoint.sh"]
